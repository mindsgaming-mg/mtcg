# MTCG Website

Codebase for MindsGaming rewards.

## Install

### Clone/Remix

The easiest way to get your own version of MTCG to edit and design yourself is to remix it on Glitch.
[Remix](https://glitch.com/edit/#!/remix/mtcg)

### Git

You can also install MTCG on your own website you will need Node and express.

### Miner

You can use our minyminer if your just looking to host your miner script.
https://glitch.com/~minymine

## Building (Remix)

- Edit

- fly.js
  **/// Client & Timer**
  Under **quickhubs** in client & timer change the **createHUB.src** to your hub url

- public/hubs
  Edit the **body** of **index.html**

- Customization
  You can now start custimizing your miner scripts, images and branding in the glitch editor to your liking!
